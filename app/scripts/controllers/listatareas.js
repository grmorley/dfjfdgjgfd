'use strict';

angular.module('listaTareasApp')
  .controller('ListatareasCtrl', function ($scope, $firebase) {
  	var connectDB = new Firebase('https://dashboardfrontenddep.firebaseio.com/tareas');
    $scope.tareas = $firebase(connectDB);

    	$scope.addTarea = function(){
    		$scope.tareas.$add({texto: $scope.textNewTarea, hecho: false});
    		$scope.textNewTarea = '';
    	};

    	$scope.other = function(){   	
    		return $scope.tareas.$getIndex().length;
    	};

    	$scope.tareasHechas = function(){
    		var cuenta = 0;
    		angular.forEach($scope.tareas, function(tarea){
    			if (tarea.hecho)
    				cuenta++;
    		});
    		return cuenta;
    	}

    	// Eleminar tareas antiguas
        $scope.removeTarea = function(){
            angular.forEach($scope.tareas, function(tarea){
                if (tarea.hecho) 
                    //$scope.tareas.$update(tarea.$id);
                    $scope.tareas.$remove(tarea.$id);
            });
        }

        // Eleminar tareas antiguas
    	$scope.updateTarea = function(){
    		angular.forEach($scope.tareas, function(tarea){
    			if (tarea.hecho) 
    				$scope.tareas.update(tarea.$id);;
    		});
    	}
  });




