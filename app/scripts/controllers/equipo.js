'use strict';

angular.module('listaTareasApp')
  .controller('EquipoCtrl', function ($scope) {
	  
    $scope.equipos = [
      {
      	nombre: 				'Oliver',
      	apellidos: 			'Arthur',
      	email: 					'oarthur@vectorsf.com',
      	categoria: 			3,
      	departamento: 	'Maquetacion',
      	responsable: 		'Jose Luis Velazquez Baro'
      },
      {
      	nombre: 				'Abraham',
      	apellidos: 			'Fernandez Lopez',
      	email: 					'afernandezl@vectorsf.com',
      	categoria: 			5,
      	departamento: 	'Maquetacion',
      	responsable: 		'Jose Luis Velazquez Baro'
      },
      {
      	nombre: 				'Francisco',
      	apellidos: 			'Martin Perez',
      	email: 					'fmartinp@vectorsf.com',
      	categoria: 			3,
      	departamento: 	'Maquetacion',
      	responsable: 		'Jose Luis Velazquez Baro'
      },
      {
      	nombre: 				'Francisco',
      	apellidos: 			'Planellas Garcia',
      	email: 					'fplanellas@vectorsf.com',
      	categoria: 			4,
      	departamento: 	'Maquetacion',
      	responsable: 		'Jose Luis Velazquez Baro'
      },
      {
      	nombre: 				'Fernando',
      	apellidos: 			'Serrano San Martin',
      	email: 					'feserrano@vectorsf.com',
      	categoria: 			3,
      	departamento: 	'Maquetacion',
      	responsable: 		'Jose Luis Velazquez Baro'
      },
      {
      	nombre: 				'Natalia',
      	apellidos: 			'Tello De Mingo',
      	email: 					'ntello@vectorsf.com',
      	categoria: 			3,
      	departamento: 	'Maquetacion',
      	responsable: 		'Jose Luis Velazquez Baro'
      },
      {
      	nombre: 				'Jose Luis',
      	apellidos: 			'Velazquez Baro',
      	email: 					'jlvelazquez@vectorsf.com',
      	categoria: 			6,
      	departamento: 	'Maquetacion',
      	responsable: 		'Javier Fernandez Romero'
      },
    ];
    $scope.orderProp = function(orden) {
    		$scope.ordenSelect = orden;
    };
  });
