'use strict';

angular.module('listaTareasApp')
  .controller('SoporteCtrl', function ($scope, $http, $compile) {
  		
  		$http({
  			method: 'POST',
  			url: 		'http://localhost/data/sendForm.php',
  			data:   'usuario:' + $scope.usuario + 'Email:'+ $scope.email+ 'Asunto: ' + $scope.asunto+ 'Comentario: ' + $scope.comentario, 
  			header: {'content-Type': 'application/x-www-form-urlencoded'}
  		})

  		//Función que envía el formulario
  		$scope.sendForm = function(){
  			$scope.success = "";
  			success(function(data, status){
  				$scope.resultado = data;
  				$scope.usuario = "";
  				$scope.email = "";
  				$scope.asunto = "";
  				$scope.comentario = "";
  				$scope.success = "Email enviado correctamente";

  			});

  			error(function(data, status){
  				$scope.data = data || 'Error al enviar email';
  				$scope.status = status;
  			});
  		}
  });
