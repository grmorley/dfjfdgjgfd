'use strict';

angular.module('listaTareasApp')
  .controller('WikiCtrl', function ($scope, $firebase, $animate) {
    var connectDB = new Firebase('https://dashboardfrontenddep.firebaseio.com/wiki');
    $scope.wikis = $firebase(connectDB);

    //Añadir nuevo proyecto.
    $scope.addNewWiki = function(){
      $scope.wikis.$add({customer: $scope.textNewCustomer, title: $scope.textNewTitle, desc: $scope.textNewDesc});
      $scope.textNewCustomer = '';
      $scope.textNewTitle = '';
      $scope.textNewDesc = '';
    }

    $scope.editWiki = function(){
      $scope.wikis.$push({customer: $scope.textNewCustomer, title: $scope.textNewTitle, desc: $scope.textNewDesc});
      $scope.textNewCustomer = '';
      $scope.textNewTitle = '';
      $scope.textNewDesc = '';
    }

    $scope.class = "back-start-view";
    
    $scope.viewDetails = function(){
        if ($scope.class === "back-start-view")
            $scope.class = "view";
         else
            $scope.class = "back-start-view";
    };
   /* $scope.formData = {};

    // Me devuelve todos los wikis al cargar el html
    $http.get('http://localhost:8080/scripts/controllers/wikis')
    .success(function(data){
        $scope.wikis = data;
        console.log(data);
    })

    //Me devuelve un error si no consigue cargar todos los wikis
    .error(function(data){
        console.log('Error Data:' + data);
    });

    // Añadir una nueva wiki in wikis
    $scope.addNewWiki = function(){
        $http.post('http://localhost:8080/scripts/controllers/wikis', $scope.formData)
            .success(function(data){
                $scope.formData = {};
                $scope.wikis = data;
                console.log(data);
            })
            // Me devuelve un error si no consigo añadir una wiki in wikis.
            .error(function(data) {
                console.log('Error Data:' + data);
            });
    };*/

  });
