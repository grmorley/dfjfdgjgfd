'use strict';

angular
  .module('listaTareasApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'firebase'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/equipo', {
        templateUrl: 'views/equipo.html',
        controller: 'EquipoCtrl'
      })
      .when('/tareas', {
        templateUrl: 'views/tareas.html',
        controller: 'ListatareasCtrl'
      })
      .when('/wiki', {
        templateUrl: 'views/wiki.html',
        controller: 'WikiCtrl'
      })
      .when('/soporte', {
        templateUrl: 'views/soporte.html',
        controller: 'SoporteCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
