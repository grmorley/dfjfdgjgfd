
var express 	= require('express');
var app 		= express();
var mongoose 	= require('mongoose');
var Schema = mongoose.Schema;

// Conexión con la base de datos
mongoose.connect('mongodb://localhost:27017/front');


// Configuración
app.configure(function() {
	// Localización de los ficheros estáticos
	app.use(express.static(__dirname + '/app'));
	// Muestra un log de todos los request en la consola		
	app.use(express.logger('dev'));	
	// Permite cambiar el HTML con el método POST					
	app.use(express.bodyParser());
	// Simula DELETE y PUT						
	app.use(express.methodOverride());

});

// Carga una vista HTML simple donde irá nuestra Single App Page
// Angular Manejará el Frontend
app.get('*', function(req, res) {						
	res.sendfile('./app/index.html');				
});

// /*----------------------------- Definición de Modelos ---------------------------------------------------*/

//Modelo de tareas
var wiki = mongoose.model('wiki',{
	customer: String,
	title: String,
	desc:  String
});



// /*----------------------------- /Definición de Modelos ---------------------------------------------------*/

// /*----------------------------- Rutas de la Api ----------------------------------------------------------*/

// GET de todas las tareas
app.get('/app/wikis', function(req, res){
		Wiki.find(function(err, wikis){
			if(err){
				res.send(err);
			}
			res.json(wikis);
		});
});

// /*----------------------------- /Rutas de la Api ----------------------------------------------------------*/

// /*----------------------------- POST de la api ----------------------------------------------------------*/

//POST que crea una tareas y la devuelve tras la creación
app.post('/app/wikis', function(req, res){
	Tarea.create({
		customer: req.body.text,
		title: req.body.text,
		desc: req.body.hecho,
		done: true
	}, function(err, tarea){
		if(err){
			res.send(err);
		}
		Tarea.find(function(err, tareas){
			if(err){
				res.send(err);
			}
			res.json(tareas);
		})
	})
});

// /*----------------------------- /POST de la api ----------------------------------------------------------*/

// /*----------------------------- DELETE de la api ----------------------------------------------------------*/


// DELETE una tarea específica y devuelve todos tras borrarlo.
app.delete('/app/wikis/:wiki', function(req, res) {		
	Wiki.remove({
		_id: req.params.wiki
	}, function(err, wiki) {
		if(err){
			res.send(err);
		}

		Wikis.find(function(err, wikis) {
			if(err){
				res.send(err);
			}
			res.json(wikis);
		});

	})
});

// /*----------------------------- /DELETE de la api ----------------------------------------------------------*/

// Escucha en el puerto 8080 y corre el server
app.listen(8080, function() {
	console.log('App listening on port 8080');
});					
