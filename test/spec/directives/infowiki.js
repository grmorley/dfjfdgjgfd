'use strict';

describe('Directive: infoWiki', function () {

  // load the directive's module
  beforeEach(module('listaTareasApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<info-wiki></info-wiki>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the infoWiki directive');
  }));
});
