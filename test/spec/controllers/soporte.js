'use strict';

describe('Controller: SoporteCtrl', function () {

  // load the controller's module
  beforeEach(module('listaTareasApp'));

  var SoporteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    SoporteCtrl = $controller('SoporteCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
