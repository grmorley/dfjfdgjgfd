'use strict';

describe('Controller: ListatareasCtrl', function () {

  // load the controller's module
  beforeEach(module('listaTareasApp'));

  var ListatareasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListatareasCtrl = $controller('ListatareasCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
